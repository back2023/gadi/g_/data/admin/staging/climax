#!/usr/bin/env bash
i=0
HOST=`hostname`
RANK=-1
while IFS= read -r line ; do
	if [[ "$line" == "$HOST" ]]; then
		RANK=$i
	fi
	i=$((i+1))
done < <(cat $PBS_NODEFILE | uniq) 	
 
export NODE_RANK=${RANK}
echo "${NODE_RANK} ${TOT_NODES} ${WORLD_SIZE} ${MASTER_ADDR} ${MASTER_PORT}"
echo "${GPU_PER_NODE} ${MAX_EPOCHS} ${OUTPUT_DIR} "
echo "=========================================================="
. "/scratch/fp0/mah900/Miniconda2023/etc/profile.d/conda.sh"
conda activate /scratch/fp0/mah900/env/climax

time python \
/g/data/wb00/admin/staging/ClimaX/src/climax/regional_forecast/train.py \
--config '/g/data/wb00/admin/staging/ClimaX/configs/regional_forecast_climax.yaml' \
--trainer.num_nodes=${TOT_NODES} \
--trainer.strategy=ddp --trainer.devices=${GPU_PER_NODE} \
--trainer.max_epochs=${MAX_EPOCHS} \
--data.root_dir='/g/data/wb00/admin/staging/weatherbench/5.625deg_npz' \
--data.region="NorthAmerica" \
--data.predict_range=72 \
--data.out_variables=['geopotential_500','temperature_850','2m_temperature'] \
--data.batch_size=16 \
--data.num_workers=1 \
--model.pretrained_path='/g/data/wb00/admin/staging/weatherbench/ClimaX-5.625deg.ckpt' \
--model.lr=5e-7 --model.beta_1="0.9" --model.beta_2="0.99" \
--model.weight_decay=1e-5
